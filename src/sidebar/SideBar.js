import './SideBar.css';
import dashboard from "./dashboard.svg"
import cart from "./cart.svg";
import catalog from "./catalog.svg";
import inventory from "./inventory.svg";
import orders from "./orders.svg";
import transactions from "./transactions.svg";
import storesList from "./storesList.svg";
import profile from "./profile.jpg";
import dropship_logo from "./dropship_logo.png";
import {  Link } from "react-router-dom";

const SideBar = () =>{

return(

     <div className={"sidebar_wrapper"} >
    <nav className={"sidebar__container"}>
      <div>
        <div className="items">
      <Link to={"/Dashboard"}>
          <img src={dropship_logo} className={"dropshiplogo"} />
        </Link>
        </div>
        <div >
          <div className={"sidebar__container__items"}>
          <Link to={"/Profile"}>
              <img src={profile} className={"profilepic"} />
              </Link>
              <Link to={"/Dashboard"}>
              <img src={dashboard} />
              </Link>
              <Link to={"/Catalog"}>
              <img src={catalog}  />
              </Link>
              <Link to={"/Inventory"}> 
              <img src={inventory} />
              </Link>
              <Link to={"/Cart"}>
              <img src={cart} />
              </Link>
              <Link to={"/Orders"}>
              <img src={orders} />
              </Link>
              <Link to={"/Transactions"}>
              <img src={transactions} />
              </Link>
              <Link to={"/Store"}>
              <img src={storesList} />
              </Link>
          </div>
        </div>
      </div>
    </nav>
    
  </div>
);
};
export default SideBar;







