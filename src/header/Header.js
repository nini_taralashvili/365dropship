import React from 'react'
import Search from './Search';
import SelectAll from './SelectAll';

const Header = ({allproducts, setAllProducts, setProducts,searchTerm,setSearchTerm,searchResults,setSearchResults,sellectAllProducts,
    unselectAll,selectedProducts,setSelectedProducts,products}) => {
    return (
        <div className={"header"}>
            <Search searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            searchResults={searchResults}
            setSearchResults={setSearchResults}
            allproducts={allproducts}
            setProducts={setProducts}
            setAllProducts={setAllProducts}
            
            />
            <SelectAll
            sellectAllProducts={sellectAllProducts}
            unselectAll={unselectAll}
            selectedProducts={selectedProducts}
             setSelectedProducts={setSelectedProducts}
            products={products}
            />
        </div>
    )
}

export default Header
