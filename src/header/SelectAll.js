import './SelectAll.css'
import Button from './Button.js';
const SelectAll = ({products,sellectAllProducts,
  unselectAll,selectedProducts}) => {


    return (
      <div className="selectAll__container">
         <div  onClick={sellectAllProducts}>
          <Button type={"Select All"} className={'selectall'}/>
          </div>
          <span className={"header__selection"}>
            selected {selectedProducts.length} out of {products.length} products
          </span>
          <div className="unselect" onClick={unselectAll}>
          {selectedProducts.length ? <Button type={"Clear All"} className={'clearall'}/> : null}
          </div>
      </div>
    );
  };
  
  export default SelectAll;