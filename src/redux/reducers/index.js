import {combineReducers} from "redux";
import {productsReducer} from "./productsReducers";

const reducers = combineReducers({
    product: productsReducer,
})

export default reducers;