
import  ProductsActionType from "../content/ProductsActionType"

const initialState = {
    originalProducts : [],
   displayProducts:[],
   sortType:"",
   searchQuery:""
}



export const productsReducer = (state=initialState,action) => {
    switch(action.type){
        case ProductsActionType.SET_ORIGINAL_PRODUCTS: 
        return {...state, originalProducts:action.payload };
        case ProductsActionType.SET_DISPLAY_PRODUCTS:
            return{...state, displayProducts: action.payload}
            case ProductsActionType.SET_SORT_TYPE:
                return{...state, sortType:action.payload}
                case ProductsActionType.SET_SEARCH_QUERY:
                return{...state, searchQuery:action.payload}

        default: return state;
    }
} 


