import ProductsActionType from "../content/ProductsActionType"


export const setOriginalProducts = (products) =>{
return {
    type: ProductsActionType.SET_ORIGINAL_PRODUCTS,

    payload: products
}

  };
  
  
export const setDisplayProducts = (products) =>{
  return {
      type: ProductsActionType.SET_DISPLAY_PRODUCTS,
  
      payload: products
  }
  
    };

  export const setSortType = (sortType) =>{
    return{
      type:ProductsActionType.SET_SORT_TYPE,
      payload: sortType
    }
  };


  export const setSearchQuery = (searchQuery) =>{
    return{
      type:ProductsActionType.SET_SEARCH_QUERY,
      payload: searchQuery
    }
  };
