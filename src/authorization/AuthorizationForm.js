import React from "react";
import { login } from "./Api.js";
import { useState, useEffect } from "react";
import "./AuthorizationForm.css";
import { useHistory } from "react-router-dom";
const AuthorizationForm = ({ isLogged, setIsLogged, token }) => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const logined = (e) => {
    e.preventDefault();
    login(email, password, setIsLogged);
  };

  useEffect(() => {
    if (token) {
      setIsLogged(true);
    } else if (!token) {
      setIsLogged(false);
    
    }
  }, [token]);

  useEffect(() => {
    if (isLogged) history.push("/catalog");
  }, [isLogged]);

  return (
    <>
      <form className="form" onSubmit={(e) => logined(e)}>
        <div>
          <input
            type="email"
            placeholder="Email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </div>

        <div>
          <input
            type="password"
            placeholder="Enter Your Password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </div>

        <div>
          <input id={"button"} type="Submit" value="Log In" />
        </div>
      </form>
    </>
  );
};

export default AuthorizationForm;
