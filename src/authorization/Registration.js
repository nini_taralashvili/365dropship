import React from "react";
import { useState} from "react";
import { registration } from "./Api.js";
import "./AuthorizationForm.css";

const Registration = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");



  
  const registered = (e) => {
    e.preventDefault();
    registration(firstName, lastName, email, password, passwordConfirmation);
  };

  return (
    <>
      <form className="form" onSubmit={(e) => registered(e)}>
        <div>
          <input
            type="text"
            placeholder="Firstname"
            value={firstName}
            onChange={(event) => setFirstName(event.target.value)}
          />
        </div>

        <div>
          <input
            type="text"
            placeholder="Lastname"
            value={lastName}
            onChange={(event) => setLastName(event.target.value)}
          />
        </div>

        <div>
          <input
            type="email"
            placeholder="Email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </div>

        <div>
          <input
            type="password"
            placeholder="Enter Your Password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </div>

        <div>
          <input
            type="password"
            placeholder="Confirm Your Password"
            value={passwordConfirmation}
            onChange={(event) => setPasswordConfirmation(event.target.value)}
          />
        </div>

        <div>
          <input id={"button"} type="Submit" value="Sign Up" />
        </div>
      </form>
    </>
  );
};

export default Registration;