import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use((req) => {
  req.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return req;
});

export const registration = async (firstName,lastName,email,password,passwordConfirmation) => {
  try {
    const result = await axios.post(SERVER_URL + "register", {firstName,  lastName, email, password,
       passwordConfirmation});
    localStorage.setItem("user", JSON.stringify(result.data.data));
    localStorage.setItem("token", result.data.data.token);
  } catch (err) {
    alert("err");
  }
};

export const login = async (email, password, setIsLogged) => {
  try {
    const result = await axios.post(SERVER_URL + "login", { email, password });
    localStorage.setItem("user", JSON.stringify(result.data.data));
    localStorage.setItem("token", result.data.data.token);
    setIsLogged(true);
  } catch (error) {
    alert("Something Went Wrong");
  }
};

export const addToCart = async (productId, qty=1) =>
  await axios.post(SERVER_V1 + "cart/add", { productId, qty });

export const getCart = async () => await axios.get(SERVER_V1 + "cart");

export const removeFromCart = async (productId) =>
  await axios.post(SERVER_V1 + "cart/remove/" + productId );

  export const updateCartItem = async (id, qty) =>
 await axios.post(SERVER_V1 + "cart/update/" + id, { qty });


 export  const creatProduct = async (data) => {
  const result = await axios.post(SERVER_V1 + 'products',data)
  return result.data.data

}

export  const updateProduct = async(id,data) => {
  const result = await axios.put(SERVER_V1 + `products/${id}` ,data)
  return result.data.data
}

export const getProduct = async (id) => {
  const result = await axios.get(SERVER_V1 +  `products/${id}`)
  return result.data.data
}

export const updateUserInformation = async (firstName,lastName,email,password,passwordConfirmation, id) =>
  await axios.put(`${SERVER_V1}/users/${id}`, firstName,lastName,email,password,passwordConfirmation);
