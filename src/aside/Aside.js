import "./Aside.css";
import ProductsList from "./ProductsList";
import Shipping from "./Shipping";
import Range from "./Range";
import { useState,useEffect } from "react";
import axios from 'axios';
const Aside = ({
  allproducts,
  setProducts,
  
}) => {

  const [categories, setCategories] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");
  const getCategories = async () =>
    axios
      .get("https://fakestoreapi.com/products/categories")
      .then((res) => setCategories(res.data));

  const filterByCategory = (items, choosenCategory) => {
    setProducts(
      items.filter(
        (product) =>
          product.category.toLowerCase() === choosenCategory.toLowerCase()
      )
    );
  };

  useEffect(() => {
    getCategories();
  }, []);

  useEffect(() => {
    if (selectedCategory) filterByCategory(allproducts, selectedCategory);
  }, [selectedCategory]);




  return (
    <aside className="aside catalog__filter">
      <ProductsList 
        title="Niche"
        
      />
      <ProductsList  title="Category"
      selectedCategory={selectedCategory}
      setSelectedCategory={setSelectedCategory}
      categories={categories}
      />
 
      <Shipping title="Ship From" />
      <Shipping title="Ship To" />
      <Shipping title="Select Supplier" />
      <Range title="Price" min="0" max="9999" From="0" To="4285" symbol="$" />
      <Range title="Profit" min="0" max="999" From="-12" To="97" symbol="%" />
    </aside>
  );
};
export default Aside;
