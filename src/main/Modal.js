import './Modal.css'
import CloseIcon from "@material-ui/icons/Close";

const Modal = ({product, handleClose}) => {
    return (
        <>
            <div id="modal-wraper">
                <div className="modal-wraper modal__elements">
                    <div className="modal-left-part">
                        <div className="modal-left-part--header">
                            <div>
                                <p>$20</p>
                                <p>RRP</p>
                            </div>
                            <div>
                                <p>{product.price}</p>
                                <p>COST</p>
                            </div>
                            <div className="modal-profit">
                                <p>20%($6)</p>
                                <p>PROFIT</p>
                            </div>
                        </div>
                        <div className="modal-img">
                            <img src={product.image} alt="image" />
                        </div>
                    </div>

                    <div className="modal-right-part">
                        <div className="modal__supplier">
                            <p>SKU# bgb-s0724578 COPY</p>
                            <p className="modal__supplier-details">
                                <span>Supplier:</span> <span>SP-Supplier115</span>
                            </p>
                        </div>
                        <p className="modal-title">{product.title}</p>
                        <button className="modal--add--To--Inventory">Add to Inventory</button>
                        <div className="modal-prduct-details">
                            <div className="modal-prduct-details-header">
                                <div className="modal-prduct-details--active">Product Details</div>
                                <div>Shopping Rates</div>
                            </div>
                            <div className="modal-description">
                                <p>{product.description}
                               
                                </p>
                            </div>
                        </div>
                    </div>
                    <CloseIcon onClick={handleClose} className={'CloseIcon'} />
                </div>
            </div>
            <div className="tint" onClick={handleClose}></div>
            </>
    )
};
     export default Modal