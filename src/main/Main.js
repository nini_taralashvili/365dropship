import "./Main.css";
import Catalog from "./Catalog.js";
import { useEffect, useState } from "react";
import axios from "axios";
import Modal from "./Modal.js"
import { useDispatch, useSelector } from "react-redux";
import {setOriginalProducts,setDisplayProducts } from "../redux/actions/productsAction";

const Main = ({
  products,
  setProducts,
  setAllProducts,
  checkedProduct,
  setCheckedProduct,
  selectedProducts,
  setSelectedProducts,
  cartItems,
  setCartItems
}) => {
  const [singleProduct, setSingleProduct] = useState(null);
  const dispatch = useDispatch();
  const originalProducts = useSelector((state) => state.product.originalProducts)

  const getProducts = async () => {
    axios.get("http://18.185.148.165:3000/api/v1/products").then((res) => {
      console.log(res.data.data);
      dispatch(setOriginalProducts(res.data.data));
      dispatch(setDisplayProducts(res.data.data));
      console.log("originalProducts",originalProducts);
      // localStorage.setOriginalProducts ("products", JSON.stringify(res.data.data));
    });
  };


  useEffect(() => {
    getProducts();
  }, []);

  const handleOpen = (data) => {
    setSingleProduct(data);
  };

  const handleClose = () => {
    setSingleProduct(null);
  };

  return (
    <div className={"main"}>
      <section>
        <Catalog
          products={products}
          checkedProduct={checkedProduct}
          setCheckedProduct={setCheckedProduct}
          handleOpen={handleOpen}
          selectedProducts={selectedProducts}
          setSelectedProducts={setSelectedProducts}
          cartItems={cartItems}
          setCartItems={setCartItems}
        />
        {singleProduct && (
          <Modal product={singleProduct} handleClose={handleClose} />
        )}
      </section>
    </div>
  );
};

export default Main;
