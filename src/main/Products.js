import React from "react";
import ProductSelect from "./ProductSelect.js";
export const Products = ({
  image,
  price,
  title,
  id,
  qty,
  handleOpen,
  description,
  selectedProducts,
  setSelectedProducts,
  cartItems,
  setCartItems
}) => {
  const handleCheckbox = () => {
    setSelectedProducts([id, ...selectedProducts]);
    if (selectedProducts.includes(id)) {
      setSelectedProducts(selectedProducts.filter((item) => item !== id));
    }
  };

  const productClicked = () => {
    handleOpen({ id, image, title, price, description });
  };

  



  return (
    <div
      className={`catalog__product ${
        selectedProducts.includes(id) ? "catalog__product--border" : ""
      }`}
    >
      <ProductSelect
        handleCheckbox={handleCheckbox}
        selectedProducts={selectedProducts}
        id={id}
        cartItems={cartItems}
        setCartItems={setCartItems}
        qty={qty}
      />
       {qty ? (
        <div className="quantity"><img src="https://img.icons8.com/material-rounded/24/000000/favorite-cart.png"/>{qty}</div>
      ) : null}
      <div className="catalog__photo" onClick={productClicked}>
        <img src={image} />
      </div>
      <div className="catalog__title">{title}</div>
      <div className="supplier">
        By: <a href="#">UK-Supplier163</a>
      </div>
      <div className="catalog__prices">
        <div className="catalog__rrp">
          <span className="price">$20</span>
          <span>RRP</span>
        </div>
        <div className="catalog__cost">
          <span className="price">${price}</span>
          <span>COST</span>
        </div>
        <div className="profit">
          <span className="price">30%($6)</span>
          <span>PROFIT</span>
        </div>
      </div>
    </div>
  );
};
export default Products;



