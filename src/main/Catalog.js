import React from 'react'
import Products from './Products.js';
import {useSelector } from "react-redux";
import './Catalog.css';

const Catalog = ({products,idList,setIdList,handleOpen,selectedProducts,setSelectedProducts,cartItems,setCartItems}) => {

    const displayProducts = useSelector((state) => state.product.displayProducts);

 return (
     
       <div className="catalog">
          
    {displayProducts.map((product) =>
  
        <Products
        image={product.imageUrl}
        title={product.title}
        price={product.price}
        idList={idList}
        setIdList={setIdList}
        id={product.id}
        description={product.description}
        handleOpen={handleOpen}
        selectedProducts={selectedProducts}
        setSelectedProducts={setSelectedProducts}
        cartItems={cartItems}
        setCartItems={setCartItems}
    />
  
    )}
 
        </div>
    )
}

export default Catalog




