import "./ProductSelect.css";
import Checkbox from "./Checkbox";
import Button from "../header/Button";
import { removeFromCart,addToCart,updateCartItem } from "../authorization/Api";
import { useLocation } from "react-router-dom";
const ProductSelect = ({  handleCheckbox,selectedProducts,id,qty,setCartItems }) => {
  const location = useLocation();
console.log(qty);
  const updateCart = async ( id, qty = 1 ) => {
    let result = [];
    let pathname = location.pathname.includes("Catalog");
    if (pathname) {
      result = await addToCart(id, qty);
    } else if (qty - 1 <= 0 && !pathname) {
      result = await removeFromCart(id);
    } else if (!pathname) {
      result = await updateCartItem(id, qty - 1);
    }
    if (result) setCartItems(result.data.data.cartItem.items);
  };

  return (
    <div className={'catalog__head'}>
      <Checkbox
        handleCheckbox={handleCheckbox}
        id={id}
        isChecked={selectedProducts.includes(id)}
      />
      <span onClick={location.pathname.includes("Catalog") ||
          location.pathname.includes("Inventory") ? () => updateCart(id,qty)  : null}
     className="button-wrapper--addToInventory" >
      <Button   className={"add-To-Inventory--hover"}    type={
            location.pathname.includes("Catalog")
              ? "add to inventory"
              : "remove from cart"
          }

         />
    </span>
    </div>
  );
};
export default ProductSelect;
