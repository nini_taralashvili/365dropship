import "./dropshipPages.css";
import Products from "../main/Products";
import { getCart, updateCartItem,removeFromCart,addToCart } from "../authorization/Api";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Header from "../header/Header";
import Aside from "../aside/Aside";
import Sort from "../header/Sort";
const Inventory = ({ selectedProducts, setSelectedProducts,
  cartItems,setCartItems, allproducts, setProducts,searchTerm, setSearchTerm, searchResults,
  setSearchResults,sellectAllProducts, setAllProducts, unselectAll, products}) => {
    
  
  const getCartItems = async () => {
    const result = await getCart();
    if (result) setCartItems(result.data.data.cartItem.items);
  };
  const location = useLocation();
  useEffect(() => {
    getCartItems();
  }, []);

  

  return (
    <>
  <Aside allproducts={allproducts} setProducts={setProducts} />
            <Header
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
              searchResults={searchResults}
              setSearchResults={setSearchResults}
              allproducts={allproducts}
              setAllProducts={setAllProducts}
              setProducts={setProducts}
              sellectAllProducts={sellectAllProducts}
              unselectAll={unselectAll}
              selectedProducts={selectedProducts}
              setSelectedProducts={setSelectedProducts}
              products={products}
            />
            <Sort products={products} setProducts={setProducts} />

<div className="catalog inventory">
      {cartItems &&
        cartItems.map((product) => (
          <>
           
            <Products
              {...product}
              product={product}
              selectedProducts={selectedProducts}
              setSelectedProducts={setSelectedProducts}
              cartItems={cartItems}
              setCartItems={setCartItems}
             
            />
           
          </>
        ))}
         </div>
    </>
  );
};

export default Inventory;