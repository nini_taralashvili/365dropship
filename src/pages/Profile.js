import './profile.css'
import { useState} from "react";
import profile from "../sidebar/profile.jpg";
import {updateUserInformation}  from "../authorization/Api";
const Profile = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [passwordConfirmation, setPasswordConfirmation] = useState("");
    const user = JSON.parse(localStorage.getItem("user"));

    const updated = (e) => {
        e.preventDefault();
        console.log(firstName,lastName, email, password, passwordConfirmation,user.id);
        updateUserInformation(firstName, lastName, email, password, passwordConfirmation,user.id);
      };




    return (
        <>
       
        <div className="profile__header">
          <div className="header-content">
              <span>
            <h2 id="profile__page__title">My Profile</h2>
            </span>
            <span className="profile__button">
            <button id="profile__button--sign">sign out</button>
            </span>
         </div>
         </div>
    
        <div className="profile__body">
            <div className="profile__nav">
                <span className="profile__nav__list">
                    <ul className="nav__elements">
                        <li id="default"><a href="#">profile</a></li>
                        <li><a href="#">billing</a></li>
                        <li id="invoice"><a href="#">invoice history</a></li>
                        <li id="deactivate"><button id="deactivate__button">deactivate account</button></li>
                        </ul>
                    </span>
                </div>

                <div className="avatar">
                <h3 className="avatar__title">profile picture</h3>
                <h3 id="personal">Personal Details</h3>
                <div className="avatar__photo">
                <img src={profile} alt="" className="avatar__img"/>
                <button id="avatar__upload__button">upload</button>
                 </div>
             </div>

                <div className="avatar__password">
                <h3 className="avatar__title change">change password</h3>
                      <div className="avatar__password__content">
                          <form className="password__form">
                              <label className="label__title">current password</label>
                              <div className="current__pass__wrapper">
                          <input type="password"  className="input" />
                           </div>
                            
                           <label className="label__title">New Password</label>
                              <div className="new__pass__wrapper">
                          <input type="password"  className="input"
                          value={password} 
                          onChange={(event) => setPassword(event.target.value)} />
                           </div>
                            
                           <label className="label__title">Confirm Password</label>
                              <div className="new__pass__wrapper">
                          <input type="password"  className="input" 
                           value={passwordConfirmation} 
                           onChange={(event) => setPasswordConfirmation(event.target.value)}/>
                           </div>
                          </form>
                            </div> 
                            </div>
                            
                            <div className="avatar__personal__details">     
                                <div className="personal__details__content">
                                <form className="personal__details__form">
                                    
                                <label className="personal__label">First Name</label>
                              <div className="FirstName__wrapper">
                          <input type="text"  className="input"  placeholder="firstName"
                           value={firstName}
                            onChange={(event) => setFirstName(event.target.value)}/>
                           </div>

                           <label className="personal__label lastName">Last Name</label>
                              <div className="LastName__wrapper">
                          <input type="text"  className="input"  placeholder="lastName"
                          value={lastName}
                           onChange={(event) => setLastName(event.target.value)}/>
                           </div>

                           <label className="personal__label select">Country</label>
                              <div className="select__wrapper">
                              <select className="input">
                              <option value="" selected >Select Country</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Australia">Australia</option>
                                <option value="Albania">Albania</option>
                                <option value="Angola">Angola</option>
                                </select>
                           </div>
                            </form>
                         </div>           
                      </div>


                            <div className="contact__info">
                            <h3 className="avatar__title contact">Contact Information</h3>
                                <div className="contact__info__content">
                                    <form className="contact__info__form">

                                    <label className="contact__label">Email</label>
                              <div className="email__wrapper">
                          <input type="email"  className="input"  placeholder="Email"
                           value={email} onChange={(event) => setEmail(event.target.value)}/>
                           </div>

                           <label className="contact__label">Skype</label>
                              <div className="skype__wrapper">
                          <input type="text"  className="input"  placeholder="Skype"/>
                           </div>

                           <label className="contact__label">Phone</label>
                              <div className="phone__wrapper">
                          <input type="text"  className="input"  placeholder="Phone"/>
                           </div>

                           <div>
                             <input id="save__button" type="Submit" value="Save Changes" 
                             onSubmit={(e) => updated(e)} />
                            </div>
                             </form>
                        </div>
                    </div>
            </div>
        </>
    )
}

export default Profile