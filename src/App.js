import "./App.css";
import SideBar from "./sidebar/SideBar.js";
import Main from "./main/Main.js";
import Header from "./header/Header.js";
import Sort from "./header/Sort";
import Aside from "./aside/Aside";
import AuthorizationForm from "./authorization/AuthorizationForm";
import Registration from "./authorization/Registration";
import Profile from "./pages/Profile";
import Dashboard from "./pages/Dashboard";
import Cart from "./pages/Cart";
import Orders from "./pages/Orders";
import Inventory from "./pages/Inventory";
import Transactions from "./pages/Transactions";
import Store from "./pages/Store";
import InsertProducts from "./pages/InsertProducts";
import { useState, useEffect } from "react";
import {BrowserRouter as Router} from 'react-router-dom';
import {
  Switch,
  Route,
  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";

function App() {
  const token = localStorage.getItem("token");
  const history = useHistory();
  const location = useLocation();
  const [products, setProducts] = useState([]);
  const [allproducts, setAllProducts] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState("");
  const [checkedProduct, setCheckedProduct] = useState([]);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [isLogged, setIsLogged] = useState(false);
  const [cartItems, setCartItems] = useState([]);
  const sellectAllProducts = () => {
    setSelectedProducts(products.map((product) => product.id));
  };
  const unselectAll = () => {
    setSelectedProducts([]);
  };

  useEffect(() => {
    if (!token) {
      history.push("/login");
      if (location.pathname.includes("register")) {
        history.push("/register");
      }
    }
  }, [token]);

  return (
    
    <div className="App">
      <SideBar />
      <Switch>
        <Route path={"/Profile"}>
          <Profile />
        </Route>
        <Route path={"/Dashboard"}>
          <Dashboard />
        </Route>
        <Route path={"/Inventory"}>
          <Inventory
            selectedProducts={selectedProducts}
            setSelectedProducts={setSelectedProducts}
            cartItems={cartItems}
            setCartItems={setCartItems}
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            searchResults={searchResults}
            setSearchResults={setSearchResults}
            allproducts={allproducts}
            setAllProducts={setAllProducts}
            setProducts={setProducts}
            sellectAllProducts={sellectAllProducts}
            unselectAll={unselectAll}
            selectedProducts={selectedProducts}
            setSelectedProducts={setSelectedProducts}
            products={products}
          />
        </Route>
        <Route path={"/Cart"}>
          <Cart />
        </Route>
        <Route path={"/Orders"}>
          <Orders />
        </Route>
        <Route path={"/Transactions"}>
          <Transactions />
        </Route>
        <Route path={"/Store"}>
          <Store />
        </Route>
        <Route path={'/InsertProducts/:productId?'}>
            <InsertProducts/>
        </Route>
        <Route path="/register">
          <Registration />
        </Route>
        <Route path="/login">
          <AuthorizationForm
            isLogged={isLogged}
            setIsLogged={setIsLogged}
            token={token}
          />
        </Route>
        <Route exact path="/">
          <Redirect to="/catalog" />
        </Route>
        <Route path="/catalog/:category?/:id?">
          <>
            <Aside allproducts={allproducts} setProducts={setProducts} />
            <Header
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
              searchResults={searchResults}
              setSearchResults={setSearchResults}
              allproducts={allproducts}
              setAllProducts={setAllProducts}
              setProducts={setProducts}
              sellectAllProducts={sellectAllProducts}
              unselectAll={unselectAll}
              selectedProducts={selectedProducts}
              setSelectedProducts={setSelectedProducts}
              products={products}
            />
            <Sort products={products} setProducts={setProducts} />
            <Main
              products={products}
              setProducts={setProducts}
              setAllProducts={setAllProducts}
              allproducts={allproducts}
              checkedProduct={checkedProduct}
              setCheckedProduct={setCheckedProduct}
              selectedProducts={selectedProducts}
              setSelectedProducts={setSelectedProducts}
              cartItems={cartItems}
              setCartItems={setCartItems}
            />
          </>
        </Route>
      </Switch>
    </div>
    
  );
}

export default App;